'use strict';

let done = 0;
const total = 10264;

const child_process = require('child_process');
const fs = require('fs');
const path = require('path');

const beautifyObj = (obj, indent=0) => {
  let ret = `${' '.repeat(indent)}{"n":"${obj.n}","t":${obj.t}`;

  if (obj.hasOwnProperty('v')) {
    ret = `${ret},"v":${JSON.stringify(obj.v)}`;
  }
  if (obj.hasOwnProperty('w')) {
    ret = `${ret},"w":${obj.w}`;
  }
  if (obj.hasOwnProperty('h')) {
    ret = `${ret},"h":${obj.h}`;
  }
  if (obj.hasOwnProperty('b')) {
    ret = `${ret},"b":${JSON.stringify(obj.b)}`;
  }
  if (obj.hasOwnProperty('x')) {
    ret = `${ret},"x":${obj.x}`;
  }
  if (obj.hasOwnProperty('y')) {
    ret = `${ret},"y":${obj.y}`;
  }
  if (obj.hasOwnProperty('c')) {
    const children = obj.c.map(c => beautifyObj(c, indent + 2)).join(',\n');
    ret = `${ret},"c":[\n${children}\n${' '.repeat(indent)}]`;
  }
  return `${ret}}`;
};

const beautify = s => beautifyObj(JSON.parse(s));

const beautifyRecurs = (inPath, outPath) => {
  if (fs.statSync(inPath).isDirectory()) {
    fs.readdirSync(inPath).forEach(f => {
      beautifyRecurs(path.join(inPath, f), path.join(outPath, f));
    });
  } else {
    const dirname = path.dirname(outPath);
    child_process.execSync(`mkdir -p ${dirname}`);
    const input = fs.readFileSync(inPath, 'utf8');
    const result = `${beautify(input)}\n`;
    fs.writeFileSync(outPath, result, 'utf8');
    done += 1;
    console.log(`${done}/${total} ${inPath} beautified`);
  }
};

beautifyRecurs('dump', 'beautified');
