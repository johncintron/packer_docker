'use strict';

const total = 10264;
let doneDump = 0;
let doneBeautified = 0;

const _ = require('lodash');
const fs = require('fs');
const path = require('path');

const validate = (p1, p2) => {
  if (fs.statSync(p1).isDirectory()) {
    fs.readdirSync(p1).forEach(f => {
      validate(path.join(p1, f), path.join(p2, f));
    });
  } else {
    const o1 = JSON.parse(fs.readFileSync(p1, 'utf8'));
    const o2 = JSON.parse(fs.readFileSync(p2, 'utf8'));
    if (!_.isEqual(o1, o2)) {
      throw new Error(`${p1} ${p2}`);
    }
    if (p1.startsWith('dump')) {
      doneDump += 1;
      console.log(`${doneDump}/${total} ${p1} validated`);
    } else {
      doneBeautified += 1;
      console.log(`${doneBeautified}/${total} ${p1} validated`);
    }
  }
};

validate('dump', 'beautified');
validate('beautified', 'dump');
